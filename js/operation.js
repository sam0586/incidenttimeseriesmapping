					var colorPicker = 0;
					var count = 0;
					var zoomCount = 0;
					var countriesCount = 0;
					var charts = [];
					var x;
					var y;
					var maxDate = '';
					var minDate = '';
					var margin = {
					    top: 10,
					    right: 40,
					    bottom: 100,
					    left: 20
					};
					var width;
					var height = $(window).height() - 300;
					var contextHeight = 30;
					var contextWidth = width;

					d3.json("data.json", function (error, data) {
					    count = 0;
					    zoomCount = 0;

					    //createDataFormat(data);
					    createChart(data)
					});


					function checkBetweenTwoDates(a, b, c) {

					    return c > a && c < b;

					}

					function checkDate(val) {
					    var update = '';
					    if (/^\d$/.test(val)) {
					        update = "0" + val
					    } else {
					        update = val;
					    }
					    return update;
					}

					width = $('#chartContainer').width() - margin.left - margin.right;
					contextHeight = 30;
					contextWidth = width;
					var svg = d3.select("#chartContainer").append("svg")
					    .attr("width", width + margin.left + margin.right)
					    .attr("height", (height + 150));

					 //d3.csv('data.csv', createChart);
					var test = 0;
					var ourData = [];



					function createChart(data) {

					    colorPicker = 0;
					    var countries = [];
					    charts = [];
					    var maxDataPoint = 0;

					    /* Loop through first row and get each country 
								and push it into an array to use later */
					    for (var prop in data[0]) {
					        if (data[0].hasOwnProperty(prop)) {
					            if (prop != 'Year') {
					                countries.push(prop);
					            }
					        }
					    };




					    countriesCount = countries.length;
					    var startYear = data[0].Year;
					    var endYear = data[data.length - 1].Year;
					    var chartHeight = height * (1 / countriesCount);

					    /* Let's make sure these are all numbers, 
							we don't want javaScript thinking it's text 
							
							Let's also figure out the maximum data point
							We'll use this later to set the Y-Axis scale
							*/
					    data.forEach(function (d) {
					        for (var prop in d) {
					            if (d.hasOwnProperty(prop)) {
					                d[prop] = d[prop];

					                if (d[prop] > maxDataPoint) {
					                    maxDataPoint = d[prop];
					                }
					            }
					        }

					        var year = d.Year.split("-")[2];
					        var month = d.Year.split("-")[1];
					        var date = d.Year.split("-")[0];
					        //console.log(new Date(d.Year,0,1));
					        //console.log(new Date(2013,15,03));
					        // D3 needs a date object, let's convert it just one time
					        d.Year = new Date(year, month - 1, date);

					    });

					    for (var i = 0; i < countriesCount; i++) {
					        charts.push(new Chart({
					            data: data.slice(),
					            id: i,
					            name: countries[i],
					            width: width,
					            height: height * (1 / countriesCount),
					            maxDataPoint: maxDataPoint,
					            svg: svg,
					            margin: margin,
					            showBottomAxis: (i == countries.length - 1)
					        }));



					    }


					    /* Let's create the context brush that will 
									let us zoom and pan the chart */

					    var contextXScale = d3.time.scale()
					        .range([0, contextWidth])
					        .domain(charts[0].xScale.domain());

					    var contextAxis = d3.svg.axis()
					        .scale(contextXScale)
					        .tickSize(contextHeight)
					        .tickPadding(-10)
					        .orient("bottom");


					    var contextArea = d3.svg.area()
					        .interpolate("monotone")
					        .x(function (d) {
					            return contextXScale(d.date);
					        })
					        .y0(contextHeight)
					        .y1(0);

					    var brush = d3.svg.brush()
					        .x(contextXScale)
					        .on("brush", onBrush);

					    svg.append("defs").append("clipPath").append("rect")
					        .attr("width", width)
					        .attr("height", height)
					    var contextXScale = d3.time.scale()
					        .range([0, contextWidth])
					        .domain(charts[0].xScale.domain());

					    var contextAxis = d3.svg.axis()
					        .scale(contextXScale)
					        .tickSize(contextHeight)
					        .tickPadding(-20)
					        .orient("bottom");

					    var contextArea = d3.svg.area()
					        .interpolate("monotone")
					        .x(function (d) {
					            return contextXScale(d.date);
					        })
					        .y0(contextHeight)
					        .y1(0);

					    var brush = d3.svg.brush()
					        .x(contextXScale)
					        .on("brush", onBrush);

					    var context = svg.append("g")
					        .attr("class", "context")
					        .attr("transform", "translate(" + (margin.left) + "," + (height + margin.top + chartHeight + 20) + ")");

					    context.append("g")
					        .attr("class", "x axis top")
					        .attr("transform", "translate(0,0)")
					        .call(contextAxis)

					    context.append("g")
					        .attr("class", "x brush")
					        .call(brush)
					        .selectAll("rect")
					        .attr("y", 0)
					        .attr("height", contextHeight);



					    function onBrush() {
					        /* this will return a date range to pass into the chart object */
					        d3.selectAll('.backgroundCircle').attr("display", "none");
					        zoomCount = 0;

					        var b = brush.empty() ? contextXScale.domain() : brush.extent();


					        for (var i = 0; i < countriesCount; i++) {
					            charts[i].showOnly(b);
					        }
					    }
					}



					function Chart(options) {
					    this.chartData = options.data;
					    this.width = options.width;
					    this.height = options.height;
					    this.maxDataPoint = options.maxDataPoint;
					    this.svg = options.svg;
					    this.id = options.id;
					    this.name = options.name;
					    this.margin = options.margin;
					    this.showBottomAxis = options.showBottomAxis;

					    var localName = this.name;
					    for (var k = 0; k < options.data.length; k++) {
					        y = d3.scale.linear().domain([0, 3]);
					        x = d3.time.scale().domain([minDate, maxDate]).range([0, 3]);
					    }




					    /* XScale is time based */
					    this.xScale = d3.time.scale()
					        .range([0, this.width])
					        .domain(d3.extent(this.chartData.map(function (d) {
					            return d.Year;
					        })));

					    /* YScale is linear based on the maxData Point we found earlier */
					    this.yScale = d3.scale.linear()
					        .range([this.height, 0])
					        .domain([0, this.maxDataPoint]);
					    var xS = this.xScale;
					    var yS = this.yScale;


					    /* 
								This is what creates the chart.
								There are a number of interpolation options. 
								'basis' smooths it the most, however, when working with a lot of data, this will slow it down 
							*/
					    this.area = d3.svg.area()
					        .interpolate("basis")
					        .x(function (d) {
					            return xS(d.Year);
					        })
					        .y0(this.height)
					        .y1(function (d) {
					            return yS(d[localName]);
					        });
					    /*
								This isn't required - it simply creates a mask. If this wasn't here,
								when we zoom/panned, we'd see the chart go off to the left under the y-axis 
							*/
					    this.svg.append("defs").append("clipPath")
					        .attr("id", "clip-" + this.id)
					        .append("rect")
					        .attr("width", this.width)
					        .attr("height", this.height);
					    /*
								Assign it a class so we can assign a fill color
								And position it on the page
							*/


					    this.chartContainer = svg.append("g")
					        .attr('class', "splineBackground")
					        .attr("transform", "translate(" + this.margin.left + "," + (this.margin.top + (this.height * this.id) + (10 * this.id)) + ")");



					    this.chartContainer.append("path")
					        .data([this.chartData])
					        .attr("class", "chart")
					        .attr("clip-path", "url(#clip-" + this.id + ")")
					        .attr("d", this.area);


					    x = d3.time.scale()
					        .domain([minDate, maxDate])
					        .range([0, width]);


					    for (i = 1; i < this.chartData.length; i++) {

					        /* We've created everything, let's actually add it to the page */



					        if (this.chartData[i][this.name] != 0) {

					            for (var m = 0; m < this.chartData[i][this.name]; m++) {
					                count++;
					                this.chartContainer.append("circle").attr("class", "backgroundCircle").attr("id", "backgroundCircle" + this.name + checkDate(this.chartData[i].Year.getDate()) + "_" + checkDate(this.chartData[i].Year.getMonth() + 1) + "_" + checkDate(this.chartData[i].Year.getFullYear()) + "_" + m).attr("key", m).attr("date", checkDate(this.chartData[i].Year.getDate()) + "_" + checkDate(this.chartData[i].Year.getMonth() + 1) + "_" + this.chartData[i].Year.getFullYear()).attr("width", "10px").attr("height", height)
					                    .attr("cx", this.xScale(this.chartData[i].Year))
					                    .attr("cy", this.yScale(this.chartData[i][this.name]) + (m * 7))
					                    .attr("r", 5)
					                    .style("fill", eventTimelineColorArray[colorPicker])
					                    .style("stroke", "#e7e7e7").attr("cursor", "pointer")
					                    .style("stroke-width", 2).on("mouseover", function () {}).on("mouseout", function () {}).on("click", function () {

					                    });
					            }
					        }

					    }

					    colorPicker = colorPicker + 1; //change the color for each event			
					    this.xAxisTop = d3.svg.axis().scale(this.xScale).orient("bottom");
					    this.xAxisBottom = d3.svg.axis().scale(this.xScale).orient("bottom");
					    /* We only want a top axis if it's the first country */
					    if (this.id == 0) {
					        /*	this.chartContainer.append("g")
											.attr("class", "x axis top")
											.attr("transform", "translate(0,0)")
											.call(this.xAxisTop);*/
					    }

					    /* Only want a bottom axis on the last country */
					    if (this.showBottomAxis) {
					        this.chartContainer.append("g")
					            .attr("class", "x axis bottom")
					            .attr("transform", "translate(0," + this.height + ")")
					            .call(this.xAxisBottom);
					    }

					    //this.yAxis = d3.svg.axis().scale(this.yScale).orient("left").ticks(5);
					    /*	
							this.chartContainer.append("g")
																	.attr("class", "y axis")
																	.attr("transform", "translate(-15,0)")
																	.call(this.yAxis);
																	
							*/
					    this.chartContainer.append("text")
					        .attr("class", "country-title")
					        .attr("transform", "translate(15,5)")
					        .text(this.name);

					}



					Chart.prototype.showOnly = function (b) {
					    minDate = b[0];
					    maxDate = b[1];

					    this.xScale.domain(b);

					    this.chartContainer.select("path").data([this.chartData]).attr("d", this.area);

					    var dragY = d3.scale.linear()
					        .range([this.height, 0])
					        .domain([0, this.maxDataPoint]);

					    for (var i = 0; i < this.chartData.length; i++) {

					        if (checkBetweenTwoDates(minDate, maxDate, this.chartData[i].Year) == true) {
					            if (this.chartData[i][this.name] != 0) {
					                for (var m = 0; m < this.chartData[i][this.name]; m++) {
					                    x = d3.time.scale()
					                        .domain([minDate, maxDate])
					                        .range([0, width]);

					                    this.chartContainer.select("#backgroundCircle" + this.name + checkDate(this.chartData[i].Year.getDate()) + "_" + checkDate(this.chartData[i].Year.getMonth() + 1) + "_" + checkDate(this.chartData[i].Year.getFullYear()) + "_" + m).attr("class", "backgroundCircle").attr("cx", x(this.chartData[i].Year)).attr("cy", dragY(this.chartData[i][this.name]) + (m * 7)).attr("display", "block");
					                    zoomCount++;
					                }
					            }
					        } else {
					            zoomCount++;
					        }
					    }
					    this.chartContainer.select(".x.axis.top").call(this.xAxisTop);
					    this.chartContainer.select(".x.axis.bottom").call(this.xAxisBottom);



					}